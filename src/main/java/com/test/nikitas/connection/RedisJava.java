package com.test.nikitas.connection;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisJava {
	public static final String CHANNEL_NAME = "redisChat";

	public static void main(String args[]) {
		JedisPool jedispool = new JedisPool("localhost");
		final Jedis subscriberJedis = jedispool.getResource();

		final RedisChatSuscriber subscriber = new RedisChatSuscriber();
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					subscriberJedis.subscribe(subscriber, CHANNEL_NAME);
					try {
						TimeUnit.SECONDS.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

		Jedis publisherJedis = jedispool.getResource();
		System.out.println("Enter user name : ");
		Scanner scanner = new Scanner(System.in);
		String userName = scanner.nextLine();
		String str = "exit";

		RedisChatPublisher chatPublisher = new RedisChatPublisher(
				publisherJedis, CHANNEL_NAME, userName);
		do {
			System.out.print("Enter your message(To exit chat type exit)");
			str = scanner.nextLine();
			chatPublisher.publishMessage(str);

		} while (!str.equals("exit"));
		scanner.close();
		subscriber.unsubscribe();
		jedispool.close();
	}
}