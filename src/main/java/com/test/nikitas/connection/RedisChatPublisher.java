package com.test.nikitas.connection;

import redis.clients.jedis.Jedis;

/**
 * This class is used for publishing messages to the redis
 * 
 * @author nikita
 *
 */
public class RedisChatPublisher {
	private final Jedis publisher;
	private final String channelName;
	private final String userName;

	public RedisChatPublisher(Jedis publisher, String channelName,
			String userName) {
		this.publisher = publisher;
		this.channelName = channelName;
		this.userName = userName;
	}

	public void publishMessage(String message) {
		publisher.publish(channelName, userName + " :: " + message);
	}
}
