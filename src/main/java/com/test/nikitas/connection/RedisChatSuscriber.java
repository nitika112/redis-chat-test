package com.test.nikitas.connection;

import redis.clients.jedis.JedisPubSub;

public class RedisChatSuscriber extends JedisPubSub {
	@Override
	public void onMessage(String channel, String message) {
		String[] splitMessage = message.split("::");
		System.err.print(splitMessage[0]);
		System.out.println(" says --> " + splitMessage[1]);
	}
}
